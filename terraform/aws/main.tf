resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDrNy0PVA5qpHDZqUAL3E3TkEVAi77vFxPYtlpCt9bPl4elQZCzv/hJuzA7Dh1XTEvpzpK9B8vbDPOBvb82n+Cmn3kKy7jjusljMoIKoq2h0Kmfw0sO4+l/YNrQydYswwpweThCIkcNLmYOZMFS2u22zJsrHypvzVVonhvnOA4IvnKUoHC/IKEEsXCZ1eVpE2S9AQPxQ4LR+gzi2eQGofwki3FDdywOrcH1uNAS+igtgFXU3cFsaGH4CywNJNeKoEOvROEAJtnQkWW+378PUXzj/SBVTZ6I8N3RQppruexc0IyPuitbsdP4Qce/of/XBTmRr7jVej45cBMitvxFnBmzkbm6BGbodJyEj+XXdqZLfPCnXwsjLXcq7zVoeOaYaUyZFwVKXV4HtCny+iXkcbnDEfO2PdN+wMsl/8ZgWoJdyP0VbqSPz6tphuREObtL3GZlaELo7NOTidh6v27c5Z4kj0u2lmRdElAG7d9w0/el1+1p0VXIve3hn4YL2iWzW4c= kali@kali"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id =aws_default_vpc.default.id

  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.deployer.key_name}"

  tags = {
    Name = "HelloWorld"
  }
}